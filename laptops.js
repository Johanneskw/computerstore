//Getting data from dataclass. 
export const getAllLaptops = () => {
    return fetch("./dataClass.json")
            .then(response => response.json())
            .then(data => data.laptops)
}
