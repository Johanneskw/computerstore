import { getAllLaptops } from './laptops.js';

class App {
    constructor() {
        this.balance = 1000;
        this.pay = 0;
        this.bankbtn = document.getElementById("bankbtn");
        this.loanbtn = document.getElementById("loanbtn");
        this.workbtn = document.getElementById("workbtn"); 
        this.buybtn = document.getElementById("buybtn");
        this.ltselector = document.getElementById("laptopselector");

        this.balancelbl = document.getElementById("balancelbl");
        this.worklbl = document.getElementById("worklbl");
        this.featureslbl = document.getElementById("featureslbl");
        this.infolbl = document.getElementById("infolbl");
        this.pricelbl = document.getElementById("pricelbl");

        this.laptops = [];
        this.sellaptop;
        this.img = new Image();
        this.hasgotloan = false; 

        //Handling loanbtn click
        this.loanbtn.onclick = () => {
            //Cant get another loan before new computer has been bought. 
            if (this.hasgotloan) alert("Dont be greedy!!");
            else {this.loanbtnclick();}
        };
        //Handling bankbtn click
        this.bankbtn.onclick = () => {
            this.bankbtnclick();
        }
        //Handling workbtn click
        this.workbtn.onclick = () => {
            this.workbtnclick();
        }
        //Handling selection of laptop
        this.ltselector.addEventListener('change', (e) => {
            this.laptopselected();
        });
        //Handles click on buy
        this.buybtn.onclick = () => {
            this.buylaptop();
        }
    }
    
    async init() {
        console.log("loading data...");
        await this.getLaptops();
    }
    
    //Gets all laptops Does not return the correct response... 
    async getLaptops() {
        try {
            this.laptops = await getAllLaptops();
        } catch (e) {
            console.error(e);
        } finally {
            this.render();
        }
    };

    //Checking if loan is valid when pressing loanbtn. 
    loanbtnclick(){
        let loanattempt = parseInt(prompt("Please enter amount of loan", (this.balance*2)));
        //Checking for invalid input. 
        if (loanattempt > (this.balance*2) || loanattempt < 0 || !Number.isInteger(loanattempt)) alert("Invalid amount.. :( please try again");
        else {
            //adds loan to balance
            this.balance += loanattempt
            console.log(this.balance);
            this.updatebalance();
            this.hasgotloan = true;
        }
    }

    updatebalance() {
        this.balancelbl.innerText = `Balance: ${this.balance}kr`;
    } 


    //adding 100 to pay for each click
    workbtnclick(){
        this.pay += 100;
        this.updatepay();
    }

    //Moves pay to balance and resets pay. updates the balance labels
    bankbtnclick(){
        this.balance += this.pay;
        this.pay = 0;
        this.updatepay();
        this.updatebalance();
    }

    updatepay(){
        this.worklbl.innerText = `Pay: ${this.pay}kr`;
    }

    //Dipslays all information about the selected laptop. 
    laptopselected(){
        console.log(this.ltselector.value);
        this.sellaptop = this.laptops[this.ltselector.value];
        this.featureslbl.innerText = this.sellaptop.feautures;
        this.img.src = this.sellaptop.image; 
        this.infolbl.innerText = this.sellaptop.description;
        this.pricelbl.innerText = `${this.sellaptop.price}kr`;
    }

    //checks if balance is high enough, then pulls laptop-price from balance.
    buylaptop(){
        if (this.balance >= this.sellaptop.price){
            alert("Congratulations on your new computer :) hope it was worth it..");
            this.balance -= this.sellaptop.price;
            this.hasgotloan = false; 
            this.updatebalance();
        } else {
            alert("Sorry, you cant afford this laptop :( back to work...");
        }
    }
    
    
    render() {
        //Updating balancelabel at start of program
        this.updatebalance();
        this.updatepay();
        //adds image to the computerstore. when selecting a laptop, the src of the image changes. 
        const imagebox = document.getElementById("imagebox");
        imagebox.appendChild(this.img);

        //adding options to selector
        const laptopnames = this.laptops.map(laptop => laptop.name);
        console.log(laptopnames);
        //Adds laptops to the selector. 
        for (let i = 0; i < laptopnames.length; i++){
            this.ltselector.options[i] = new Option(laptopnames[i], i);
        }
        //Adding data to gui
        this.laptopselected();
    }
}

new App().init();
